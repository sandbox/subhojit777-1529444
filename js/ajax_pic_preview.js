(function($) {
  function ajaxFileUpload(context, settings) {
      $("#loading")
      .ajaxStart(function(){
        $(this).show();
        })
      .ajaxComplete(function(){
        $(this).hide();
        });
      var mode = Drupal.settings.ajax_pic_preview.mode;
      var upload_message = Drupal.settings.ajax_pic_preview.upload_message;

      $.ajaxFileUpload({
        url: '?q=ajax-pic-preview/upload/' + mode,
        secureuri:false,
        fileElementId:'edit-picture-upload',
        dataType: 'json',
        success: function (data, status){
          if (typeof(data.error) != 'undefined') {
            if (data.error != '') {
              $('#edit-picture-message').html(data.error);
              $('#edit-picture-upload').change(ajaxFileUpload);
            }
            else {
              $('#edit-picture-message').html(upload_message);
              if ($('#img-picture').html() == null) {
                $('fieldset#edit-picture div.fieldset-wrapper').append('<div class="picture"><a href="#" title="View user profile."><img src="" id="img-picture"></a></div>');
              }
              $('#img-picture').attr('src', data.img);
              $('#edit-picture-upload').change(ajaxFileUpload);
            }
          }
        },
        error: function (data, status, e) {
          /*alert(e);*/
        }
      });

      return true;
  }

  Drupal.behaviors.ajax_pic_preview = {
    attach: function(context, settings)  {
      $(document).ready(function() {
        $('#user-profile-form .picture img').attr('id', 'img-picture');
        $('#user-profile-form .picture img').width(Drupal.settings.preview_image_width);
        $('label[for="edit-picture-upload"]').append('<span id="edit-picture-message" class="form-required"></span>');
        $('#edit-picture-upload').change(ajaxFileUpload).after('<span id="loading" style="display:none"><img src='+Drupal.settings.basePath+'misc/throbber.gif /></span>');
        ajaxFileUpload(context, settings);
      });
    }
  };
})(jQuery);
