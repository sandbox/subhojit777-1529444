This module uses JQuery and the AJAX File Upload Jquery plugin to present
a preview of a user's new picture as soon as they select one.  Uploads will be
sent through the same validation process and errors will be reported immediately
after selection has been made.

Important! Make sure cron is setup properly or your pictures/tmp/ directory
will grow very large. Preview images are staged there and cron will remove them
once the originating session has expired.


Installation
------------
Copy ajax_pic_preview.module to your module directory and then enable on the
admin modules page. Make sure pictures is enabled in your User Settings.


Troubleshooting
---------------
If the module isn't working as expected, make sure that there is a subdirectory
in your pictures directory named 'tmp' with the appropriate permissions
